<?php

/**
 * @file
 * Contains the code to generate the custom drush commands.
 */

/**
 * Implements hook_drush_command().
 *
 * The call back function names are in this format:
 *   drush_{module_name}_{item_id_for_command}()
 */
function hfc_course_equiv_drush_command() {
  return [
    'exclude-ce-broken' => [
      'description' => 'Exclude broken course equivalencies from search results.',
      'drupal dependencies' => ['hfc_course_equiv'],
    ],
    'exclude-ce-byid' => [
      'description' => 'Exclude course equivalencies by source college ID.',
      'drupal dependencies' => ['hfc_course_equiv'],
      'arguments' => [
        'id' => 'The college HANK ID to exclude.',
      ],
    ],
    'exclude-ce-byname' => [
      'description' => 'Exclude course equivalencies by source college name.',
      'drupal dependencies' => ['hfc_course_equiv'],
      'arguments' => [
        'name' => 'The college name to exclude.',
      ],
    ],
  ];
}

/**
 * Call back for exclude-ce-broken command.
 */
function drush_hfc_course_equiv_exclude_ce_broken() {
  Drupal::service('course_equivs')->excludeBroken();
}

/**
 * Call back for exclude-ce-broken command.
 */
function drush_hfc_course_equiv_exclude_ce_byid($id = NULL) {
  if (is_null($id)) {
    Drupal::messenger()->addError("Source college ID required!");
    return;
  }
  Drupal::service('course_equivs')->excludeCollegeId($id);
}

/**
 * Call back for exclude-ce-broken command.
 */
function drush_hfc_course_equiv_exclude_ce_byname($name = NULL) {
  if (is_null($id)) {
    Drupal::messenger()->addError("Source college name required!");
    return;
  }
  Drupal::service('course_equivs')->excludeCollegeName($name);
}
