<?php

/**
 * @file
 * Contains course_equiv.page.inc.
 *
 * Page callback for Course Equivalency entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Course Equivalency templates.
 *
 * Default template: course_equiv.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_course_equiv(array &$variables) {
  // Fetch CourseEquiv Entity Object.
  $course_equiv = $variables['elements']['#course_equiv'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
