<?php

namespace Drupal\hfc_course_equiv;

/**
 * Defines the Course Equivalency Service Interface.
 */
interface CourseEquivServiceInterface {

  /**
   * Exclude records by Source College ID.
   *
   * @param int $id
   *   The ID to exclude.
   */
  public function excludeCollegeId($id);

  /**
   * Exclude records by Source College Name.
   *
   * @param string $name
   *   The name to exclude.
   */
  public function excludeCollegeName($name);

  /**
   * Exclude broken records.
   */
  public function excludeBroken();

  /**
   * Get a list of source schools to filter results.
   */
  public function getSourceSchools();

  /**
   * Display results of lookup by school.
   *
   * @param string[] $values
   *   The search form values.
   *
   * @return array
   *   A renderable array.
   */
  public function showResults($values);

}
