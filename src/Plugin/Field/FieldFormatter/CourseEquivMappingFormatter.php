<?php

namespace Drupal\hfc_course_equiv\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'course_equiv_mapping_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "course_equiv_mapping_formatter",
 *   label = @Translation("Course Equivalency Mapping"),
 *   field_types = {
 *     "course_equiv_mapping"
 *   }
 * )
 */
class CourseEquivMappingFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $src_courses = [];
    $src_credits = 0;
    $hfc_courses = [];
    $hfc_credits = 0;

    foreach ($items as $ls) {

      $src_course = $this->t('@id @title', [
        '@id' => $ls->src_course,
        '@title' => $ls->src_title,
      ]);
      $hfc_course = $this->t('@sub @num @title', [
        '@sub' => $ls->hfcc_course_subject,
        '@num' => $ls->hfcc_course_number,
        '@title' => $ls->hfcc_title,
      ]);

      if (!in_array($src_course, $src_courses)) {
        $src_courses[] = $src_course;
        $src_credits += $ls->src_credits;
      }
      if (!in_array($hfc_course, $hfc_courses)) {
        $hfc_courses[] = $hfc_course;
        $hfc_credits += $ls->hfcc_credits;
      }
    }

    sort($src_courses);
    sort($hfc_courses);

    $row = [
      'src_course' => ['data' => ['#markup' => implode('<br>', $src_courses)]],
      'src_credits' => ['data' => ['#markup' => number_format($src_credits, 3)]],
      'hfc_course' => ['data' => ['#markup' => implode('<br>', $hfc_courses)]],
      'hfc_credits' => ['data' => ['#markup' => number_format($hfc_credits, 3)]],
    ];

    return [
      '#theme' => 'table',
      '#rows' => [$row],
      '#header' => [
        $this->t('Course'),
        $this->t('Credits'),
        $this->t('HFC Course'),
        $this->t('HFC Credits'),
      ],
      '#empty' => $this->t('No matching records found for this source.'),
    ];
  }

}
