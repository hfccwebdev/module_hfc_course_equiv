<?php

namespace Drupal\hfc_course_equiv\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'course_equiv_mapping' field type.
 *
 * @FieldType(
 *   id = "course_equiv_mapping",
 *   label = @Translation("Course Equivalency Mapping"),
 *   description = @Translation("Provides a mapping from external course to HFC course."),
 *   default_widget = "course_equiv_mapping_widget",
 *   default_formatter = "course_equiv_mapping_formatter",
 * )
 */
class CourseEquivMapping extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['src_course'] = DataDefinition::create('string')
      ->setLabel(t('Transfer Course'))
      ->setDescription(t('The name of the course to transfer.'))
      ->setRequired(TRUE);

    $properties['src_title'] = DataDefinition::create('string')
      ->setLabel(t('Transfer Course Title'))
      ->setDescription(t('The title of the course to transfer.'))
      ->setRequired(TRUE);

    $properties['src_credits'] = DataDefinition::create('string')
      ->setLabel(t('Transfer Credits'))
      ->setDescription(t('The number of credits for the transfer course.'))
      ->setRequired(TRUE);

    // This does _not_ have to be an entity reference for what we want it to do.
    $properties['hfcc_course_subject'] = DataDefinition::create('string')
      ->setLabel(t('HFC Course Subject'))
      ->setDescription(t('The subject of the equivalent HFC course.'))
      ->setRequired(TRUE);

    $properties['hfcc_course_number'] = DataDefinition::create('string')
      ->setLabel(t('HFC Course Number'))
      ->setDescription(t('The number of the equivalent HFC course.'))
      ->setRequired(TRUE);

    $properties['hfcc_title'] = DataDefinition::create('string')
      ->setLabel(t('HFC Course Title'))
      ->setDescription(t('The title of the equivalent HFC course.'))
      ->setRequired(TRUE);

    $properties['hfcc_credits'] = DataDefinition::create('string')
      ->setLabel(t('HFC Credits'))
      ->setDescription(t('The number of credits for the HFC course.'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'src_course' => [
          'type' => 'varchar',
          'length' => '20',
        ],
        'src_title' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'src_credits' => [
          'type' => 'numeric',
          'precision' => '10',
          'scale' => '3',
        ],
        'hfcc_course_subject' => [
          'type' => 'varchar',
          'length' => '20',
        ],
        'hfcc_course_number' => [
          'type' => 'varchar',
          'length' => '20',
        ],
        'hfcc_title' => [
          'type' => 'varchar',
          'length' => '255',
        ],
        'hfcc_credits' => [
          'type' => 'numeric',
          'precision' => '10',
          'scale' => '3',
        ],
      ],
    ];
  }

}
