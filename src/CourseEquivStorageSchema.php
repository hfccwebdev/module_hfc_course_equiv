<?php

namespace Drupal\hfc_course_equiv;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Defines the course_equiv schema handler.
 */
class CourseEquivStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    if ($base_table = $this->storage->getBaseTable()) {
      $schema[$base_table]['indexes'] += [
        'src_college' => ['src_college'],
        'src_college_id' => ['src_college_id'],
      ];
    }
    return $schema;
  }

}
