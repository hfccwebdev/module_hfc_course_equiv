<?php

namespace Drupal\hfc_course_equiv\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hfc_course_equiv\CourseEquivServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CourseEquivLookupForm.
 */
class CourseEquivLookupForm extends FormBase {

  /**
   * Drupal\hfc_course_equiv\CourseEquivServiceInterface definition.
   *
   * @var \Drupal\hfc_course_equiv\CourseEquivServiceInterface
   */
  protected $courseEquiv;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('course_equivs')
    );
  }

  /**
   * Constructs a new NewCourseSectionsForm object.
   */
  public function __construct(
    CourseEquivServiceInterface $course_equiv
  ) {
    $this->courseEquiv = $course_equiv;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'course_equiv_lookup_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $schools = $this->courseEquiv->getSourceSchools();

    $form['src_college_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Transfer College/University'),
      '#empty_option' => t('- select a school -'),
      '#options' => $schools,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Show courses'),
    ];

    $form['output'] = [
      '#prefix' => '<div class="results">',
      'results' => $this->courseEquiv->showResults($form_state->getValues()),
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
