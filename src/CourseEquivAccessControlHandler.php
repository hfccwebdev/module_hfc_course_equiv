<?php

namespace Drupal\hfc_course_equiv;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Course Equivalency entity.
 *
 * @see \Drupal\hfc_course_equiv\Entity\CourseEquiv.
 */
class CourseEquivAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\hfc_course_equiv\Entity\CourseEquivInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowed();

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit course equivalency entities');

      case 'delete':

        return AccessResult::forbidden();
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::forbidden();
  }

}
