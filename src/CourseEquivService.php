<?php

namespace Drupal\hfc_course_equiv;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_course_equiv\Entity\CourseEquiv;
use Drupal\hfc_course_equiv\Entity\CourseEquivInterface;

/**
 * Defines the Course Equivalency Service.
 */
class CourseEquivService implements CourseEquivServiceInterface {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * Stores the Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Stores the database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new service object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function excludeCollegeId($id) {
    $query = $this->entityTypeManager->getStorage('course_equiv')->getQuery()->accessCheck(FALSE);
    $query->condition('src_college_id', $id);
    if ($result = $query->execute()) {
      array_map('self::processExclusions', $result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function excludeCollegeName($name) {
    $query = $this->entityTypeManager->getStorage('course_equiv')->getQuery()->accessCheck(FALSE);
    $query->condition('src_college', $name);
    if ($result = $query->execute()) {
      array_map('self::processExclusions', $result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function excludeBroken() {

    $query = $this->database->select('hank_course_equivs', 'eq');
    $query->leftJoin('course_equiv__course_equiv_ls', 'ls', "eq.course_equivs_id = ls.entity_id");
    $query->leftJoin('course_equiv__field_exclude', 'x', "eq.course_equivs_id = x.entity_id");

    $query->fields('eq', ['course_equivs_id']);

    $query->isNull('ls.delta');

    $excludes = $query->orConditionGroup()
      ->condition('x.field_exclude_value', 0)
      ->isNull('x.field_exclude_value');
    $query->condition($excludes);

    if ($result = $query->execute()->fetchCol()) {
      array_map('self::processExclusions', $result);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceSchools() {
    $query = $this->database->select('hank_course_equivs', 'eq');

    // Optional: Suppress asterisks added in HANK to denote valid entries.
    $query->fields('eq', ['src_college_id']);
    $query->addExpression("REPLACE(src_college, '*', '')", 'src_college');

    $query->leftJoin('course_equiv__field_exclude', 'x', "eq.course_equivs_id = x.entity_id");

    $excludes = $query->orConditionGroup()
      ->condition('x.field_exclude_value', 0)
      ->isNull('x.field_exclude_value');
    $query->condition($excludes);

    $query->condition('eq.src_college', '.', '<>');

    $query->orderBy('eq.src_college');
    $query->orderBy('eq.src_college_id');

    return $query->execute()->fetchAllKeyed();
  }

  /**
   * {@inheritdoc}
   */
  public function showResults($values) {

    if (!empty($values['src_college_id'])) {
      $query = $this->entityTypeManager->getStorage('course_equiv')->getQuery()->accessCheck(FALSE);
      $query->condition('src_college_id', $values['src_college_id']);

      $excludes = $query->orConditionGroup()
        ->condition('field_exclude', 0)
        ->notExists('field_exclude');
      $query->condition($excludes);

      if ($result = $query->execute()) {
        $entities = CourseEquiv::loadMultiple($result);

        $rows = [];
        foreach ($entities as $entity) {

          if (!empty($entity->course_equiv_ls[0])) {
            $row_id = $entity->course_equiv_ls[0]->src_course . "-" . $entity->id();
            $rows[$row_id] = [
              'data' => $this->buildTableRow($entity),
              'id' => ["equiv-{$entity->id()}"],
            ];
          }
          else {
            $message = 'Course Equivalency %id for %src (%sid) contains no course information.';
            $values = [
              '%id' => $entity->id(),
              '%src' => $entity->src_college->value,
              '%sid' => $entity->src_college_id->value,
            ];
            $this->getLogger('hfc_catalog_workflow')->warning($message, $values);
          }
        }

        // Now sort all the rows by the first course in the equivalency.
        ksort($rows);

        return [
          '#theme' => 'table',
          '#rows' => $rows,
          '#header' => [
            $this->t('Course'),
            $this->t('Credits'),
            $this->t('HFC Course'),
            $this->t('HFC Credits'),
          ],
          '#empty' => $this->t('No matching records found for this source.'),
        ];
      }
    }
  }

  /**
   * Builds a table row for displaying course equivalencies.
   *
   * This convolution is required to squash everything
   * into a single table row when an equivalency
   * represents multiple courses.
   *
   * @param \Drupal\hfc_course_equiv\Entity\CourseEquivInterface $entity
   *   The source entity.
   *
   * @return array
   *   An array of renderable table columns.
   */
  private function buildTableRow(CourseEquivInterface $entity) {

    $src_courses = [];
    $src_credits = 0;
    $hfc_courses = [];
    $hfc_credits = 0;

    foreach ($entity->course_equiv_ls as $ls) {

      $src_course = $this->t('@id @title', [
        '@id' => $ls->src_course,
        '@title' => $ls->src_title,
      ]);
      $hfc_course = $this->t('@sub @num @title', [
        '@sub' => $ls->hfcc_course_subject,
        '@num' => $ls->hfcc_course_number,
        '@title' => $ls->hfcc_title,
      ]);

      if (!in_array($src_course, $src_courses)) {
        $src_courses[] = $src_course;
        $src_credits += $ls->src_credits;
      }
      if (!in_array($hfc_course, $hfc_courses)) {
        $hfc_courses[] = $hfc_course;
        $hfc_credits += $ls->hfcc_credits;
      }
    }

    sort($src_courses);
    sort($hfc_courses);

    return [
      'src_course' => ['data' => ['#markup' => implode('<br>', $src_courses)]],
      'src_credits' => ['data' => ['#markup' => number_format($src_credits, 3)]],
      'hfc_course' => ['data' => ['#markup' => implode('<br>', $hfc_courses)]],
      'hfc_credits' => ['data' => ['#markup' => number_format($hfc_credits, 3)]],
    ];
  }

  /**
   * Exclude specified records.
   */
  private function processExclusions($id) {
    if ($entity = CourseEquiv::load($id)) {

      $message = 'Course Equivalency %id for %src (%sid) marked as excluded.';
      $values = [
        '%id' => $entity->id(),
        '%src' => $entity->src_college->value,
        '%sid' => $entity->src_college_id->value,
      ];
      $this->getLogger('hfc_catalog_workflow')->warning($message, $values);

      $entity->field_exclude->setValue(TRUE);
      $entity->save();
    }
  }

}
