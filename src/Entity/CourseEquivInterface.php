<?php

namespace Drupal\hfc_course_equiv\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Course Equivalency entities.
 *
 * @ingroup hfc_course_equiv
 */
interface CourseEquivInterface extends ContentEntityInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Course Equivalency name.
   *
   * @return string
   *   Name of the Course Equivalency.
   */
  public function getName();

}
