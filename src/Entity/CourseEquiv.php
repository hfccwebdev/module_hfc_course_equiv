<?php

namespace Drupal\hfc_course_equiv\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the Course Equivalency entity.
 *
 * @ingroup hfc_course_equiv
 *
 * @ContentEntityType(
 *   id = "course_equiv",
 *   label = @Translation("Course Equivalency"),
 *   handlers = {
 *     "storage_schema" = "Drupal\hfc_course_equiv\CourseEquivStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\hfc_course_equiv\CourseEquivListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\hfc_course_equiv\Form\CourseEquivForm",
 *       "edit" = "Drupal\hfc_course_equiv\Form\CourseEquivForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\hfc_course_equiv\CourseEquivHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\hfc_course_equiv\CourseEquivAccessControlHandler",
 *   },
 *   base_table = "hank_course_equivs",
 *   translatable = FALSE,
 *   admin_permission = "administer course equivalency entities",
 *   entity_keys = {
 *     "id" = "course_equivs_id",
 *     "src_college_id" = "src_college_id",
 *     "src_college" = "src_college",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/hfc/course-equiv/list/{course_equiv}",
 *     "edit-form" = "/admin/config/hfc/course-equiv/list/{course_equiv}/edit",
 *   },
 *   field_ui_base_route = "course_equiv.settings"
 * )
 */
class CourseEquiv extends ContentEntityBase implements CourseEquivInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    $name = $this->get('src_college')->value;
    return $this->t('@college Course Equivalency @id', [
      '@college' => preg_replace('/\*/', '', $name),
      '@id' => $this->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getName();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['course_equivs_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Course Equivs ID'))
      ->setDescription(t('The ID of the Course Equivalency entity.'))
      ->setReadOnly(TRUE);

    $fields['src_college'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Source College'))
      ->setDescription(t('The name of the college to transfer courses from.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['src_college_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source College ID'))
      ->setDescription(t('The ID of the college to transfer courses from.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['course_equiv_ls'] = BaseFieldDefinition::create('course_equiv_mapping')
      ->setLabel(t('Equivalent Course(s)'))
      ->setDescription(t('Defines a course equivalency grouping.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setCardinality(-1);

    $fields['hfcc_end_date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('HFC End Date'))
      ->setDescription(t('The end date credit will be accepted.'))
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
